/*******************************************************************************
 * Ryan DeBraal <ryan.debraal@snhu.edu>
 *******************************************************************************/
package org.eclipse.example.calc.internal.operations;

import org.eclipse.example.calc.BinaryOperation;
import java.lang.Math;

/**
 * Binary power operation
 */
public class Power extends AbstractOperation implements BinaryOperation {

	@Override
	public float perform(float arg1, float arg2) {
		//TODO: Find a way of doing exponents without datatype conversion
		return (float)Math.pow((double)arg1, (double)arg2);
	}

	@Override
	public String getName() {
		return "^";
	}

}
